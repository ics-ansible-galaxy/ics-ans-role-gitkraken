import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

VERSION = '6.3.0'


def test_gitkraken_installed(host):
    gitkraken_bin = host.file('/opt/gitkraken-{}/gitkraken'.format(VERSION))
    assert gitkraken_bin.is_file
    assert gitkraken_bin.mode == 0o755
