ics-ans-role-gitkraken
=======================

Ansible role to install [GitKraken](https://www.gitkraken.com).

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
gitkraken_version: 6.3.0
gitkraken_archive: https://artifactory.esss.lu.se/artifactory/swi-pkg/gitkraken/{{ gitkraken_version }}/gitkraken-amd64.tar.gz
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-gitkraken
```

License
-------

BSD 2-clause
